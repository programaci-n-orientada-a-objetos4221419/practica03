
import Controlador.Controlador;
import Modelo.Bomba;
import Vista.dlgBomba;
/**
 *
 * @author Estrella
 */
public class Main {
    public static void main(String[] args) {
        Bomba bomba = new Bomba();
        dlgBomba vista = new dlgBomba();
        
       Controlador controlador = new Controlador(bomba, vista);
        controlador.iniciarVista();
    } 
}


package Controlador;

import Modelo.Bomba;
import Modelo.Gasolina;
import Vista.dlgBomba;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static java.lang.Float.parseFloat;
import static java.lang.Integer.parseInt;
import static java.lang.String.valueOf;
import java.awt.Dimension;

public class Controlador implements ActionListener {
      private Bomba bomba;
    private dlgBomba vista;
    private HashMap<String, Float> preciosGasolina;
    private int contadorVentas;

    public Controlador(Bomba bomba, dlgBomba vista) {
        this.bomba = bomba;
        this.vista = vista;
        this.vista.btnIniciarBomba.addActionListener(this);
        this.vista.btnregistrar.addActionListener(this);
        this.vista.jComboBox1.addActionListener(this);
        preciosGasolina = new HashMap<>();
        preciosGasolina.put("Premium", 15.0f);
        preciosGasolina.put("Diesel", 12.5f);
        preciosGasolina.put("Regular", 10.0f);
        
        String[] tiposGasolina = preciosGasolina.keySet().toArray(new String[preciosGasolina.size()]);

        contadorVentas = 0;
        
        this.vista.jComboBox1.setModel(new DefaultComboBoxModel(tiposGasolina));
        this.vista.txtVentas.setText(String.valueOf(contadorVentas));
        this.vista.txtPrecioVenta.setText(String.valueOf(preciosGasolina.get((String) this.vista.jComboBox1.getSelectedItem())));
    }

    public void iniciarVista() {
        vista.setTitle("Gas de Mexico");
        vista.setMinimumSize(new Dimension(600, 400));
        vista.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        vista.pack();
        vista.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (vista.btnIniciarBomba == e.getSource()) {
            iniciarBomba();
        } else if (vista.btnregistrar == e.getSource()) {
            registrarVenta();
        } else if (vista.jComboBox1 == e.getSource()){
            this.vista.txtPrecioVenta.setText(valueOf(preciosGasolina.get(vista.jComboBox1.getSelectedItem())));
        }
    }
    
    private void iniciarBomba() {
        int numeroBomba;
        
        try{
            numeroBomba = parseInt(this.vista.TxtBomba.getText());
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Inserte numero de bomba");
            return;
        } 
                
        float precioGasolina = parseFloat(this.vista.txtPrecioVenta.getText());

        Gasolina gasolina = new Gasolina();
        gasolina.setTipo(valueOf(preciosGasolina.get(vista.jComboBox1.getSelectedItem())));
        gasolina.setPrecio(precioGasolina);
        
        this.bomba.setNumeroBomba(numeroBomba);
        this.bomba.setGasolina(gasolina);
        this.bomba.setCapacidad(this.vista.jSlider1.getValue());
        
        JOptionPane.showMessageDialog(null, "Se ha iniciado la bomba " + numeroBomba);
    }
    
     private void registrarVenta() {
        int cantidad;
        try{
            cantidad = parseInt(this.vista.txtcatidad.getText());
        }catch (Exception e){
            JOptionPane.showMessageDialog(null, "Inserte cantidad a vender");
            return;
        }
        
        float importeVenta = bomba.venderGasolina(cantidad);

        if (importeVenta > 0) {
            this.contadorVentas++;
            this.vista.txtVentas.setText(valueOf(contadorVentas));
            this.vista.txtcosto.setText(valueOf(importeVenta));
            this.vista.txttotalventas.setText(valueOf(bomba.ventasTotales()));
        } else {
            JOptionPane.showMessageDialog(null, "No hay suficiente gasolina disponible.");
        }

        this.vista.jSlider1.setValue(bomba.getCapacidad());
    }
}

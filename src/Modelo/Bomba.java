
package Modelo;

/**
 *
 * @author Estrella
 */
public class Bomba {
    private int numeroBomba;
    private Gasolina gasolina;
    private int capacidad;
    private int acumuladorLitros;
     public int getNumeroBomba() {
        return numeroBomba;
    }

    public void setNumeroBomba(int numeroBomba) {
        this.numeroBomba = numeroBomba;
    }

    public Gasolina getGasolina() {
        return gasolina;
    }

    public void setGasolina(Gasolina gasolina) {
        this.gasolina = gasolina;
    }

    public int getCapacidad() {
        return capacidad;
    }
    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public void iniciarBomba(int numeroBomba, Gasolina gasolina, int capacidad) {
        this.numeroBomba = numeroBomba;
        this.gasolina = gasolina;
        this.capacidad = capacidad;
        this.acumuladorLitros = 0;
    }
    public float venderGasolina(int cantidad) {
        if (cantidad <= capacidad) {
            acumuladorLitros += cantidad;
            capacidad -= cantidad;
            return cantidad * gasolina.getPrecio();
        } else {
            return 0;
        }
    }

    public float ventasTotales() {
        return acumuladorLitros * gasolina.getPrecio();
    }

    public void setCapacidad(float capacidad) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
}

